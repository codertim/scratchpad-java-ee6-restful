
package net.timwforce;

//import javax.ejb.LocalBean;
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.PersistenceContextType;
//import javax.persistence.Query;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ejb.EJB;
// import net.timwforce.RestfulPracticeApiSessionBean;
import net.timwforce.TestJson;


@Path("/mypath")
public class RestfulJsonPractice {
	@EJB 
	private RestfulPracticeApiSessionBean restuflPracticeApiSessionBean;

	// @Produces(MediaType.APPLICATION_JSON)

	@GET
	@Path("jsonstuff")
	@Produces("application/json")
	public TestJson getJson() {
		// float currentTemp = restfulPracticeApiSessionBean.getCurrentTemp();
		TestJson testJson = new TestJson();
		return testJson;
    	}

	@GET
	@Path("stuff")
	@Produces("text/html")
	public String getHtml() {
		// float currentTemp = restfulPracticeApiSessionBean.getCurrentTemp();
		float currentTemp = 98.3f;
		return "<html lang=\"en\"><body><h1>Hello There, Rest World!! " + currentTemp +  " </body></h1></html>";
    	}
}



